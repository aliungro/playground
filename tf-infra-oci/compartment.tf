resource "oci_identity_compartment" "oci_compartment" {
  compartment_id = var.tenancy_id
  description    = "Compartment for Terraform resources."
  name           = "compartment_for_TF"
}