terraform {
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = "5.32.0"
    }
    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }
  }
  required_version = ">= 0.13"
}