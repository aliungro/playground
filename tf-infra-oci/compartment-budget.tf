resource "oci_budget_budget" "tf_compartment_budget" {
  amount         = 1
  compartment_id = oci_identity_compartment.oci_compartment.compartment_id
  reset_period   = "MONTHLY"

  #Optional
  description                           = "Budget TF"
  display_name                          = "tf_budget"
  processing_period_type                = "MONTH"
  budget_processing_period_start_offset = 1
  # target_compartment_id = oci_identity_compartment.oci_compartment.compartment_id
  target_type = "COMPARTMENT"
  targets     = [oci_identity_compartment.oci_compartment.compartment_id]
}

resource "oci_budget_alert_rule" "alert_rule_1_leu" {
  budget_id      = oci_budget_budget.tf_compartment_budget.id
  threshold      = 1
  threshold_type = "ABSOLUTE"
  type           = "ACTUAL"
  description    = "alert consume"
  display_name   = "alert_consume"
  message        = "high consume"
  recipients     = "ungureanualin123@yahoo.com"
}