# TF CLOUD
if you want to use TF CLOUD please add your org name and workspace in the terraform.tf file

# SSH KEY VALIDATION
To have a SSH key valid for oci you will need to do the following:
- create ssh key locally
- go to oci UI
- select your profile (upper right)
- select from the menu API keys
- add API key
- select -> paster public key
- cat the public key that you created and add it here
- copy all the ids that oci will provide for you

# Variables
You can set the variables directly on the variables.tf or you can use TF CLOUD for them. Using vars is important (also setting them up) because oci tends to confuse ids of different resources -> this will result in failed applies. You can take the vars from the oci UI.
Examples of vars for context:
- tenancy_id = ocid1.tenancy.oc1..aaaaaaaatuzole...
- user_id = ocid1.user.oc1..aaaaaaaazif2wgifzj...
- api_fingerprint - 1a:c1:61:f2:f9:3b:e5:11:05:7... (this is releated to your ssh key)
- region = eu-frankfurt-1 (the region you selected when you created the account)
- private_key = you need to generate it (local or oci UI), you can also point to a local file directory -> use /home/alin/.oci/...
```
variable "private_key" {
  type    = string
  default = <<EOF
-----BEGIN PRIVATE KEY-----
-----END PRIVATE KEY-----
EOF
}
```
- compartment_id_var = ocid1.compartment.oc1..aaaaaaaa6n3vzvoxku...
- image_id_oci = ocid1.image.oc1.eu-frankfurt-1.aaaaaaaazmivdg6hi272oidkvp7rjmnpf5ay7m7tipx3nurps6znn4neoqoq (this is ubuntu)
- subnet_id_oci = ocid1.subnet.oc1.eu-frankfurt-1.aaaaaaaafxw7lcb6esxf
- ssh_auth_key_1 = ssh-rsa AAAAB3NzaC1yc2EAAAADAQ... (public key for instances/vm) -> created local
- ssh_auth_key_2 = ssh-rsa AAAAB3NzaC1yc2EAAAADAQ... (public key for instances/vm) -> created local
- availability_domain_oci_europe_frankfurt_1_ad_2 = Dayw:EU-FRANKFURT-1-AD-2 -> change this to Dayw:EU-FRANKFURT-1-AD-1 or Dayw:EU-FRANKFURT-1-AD-3 if you get errors at apply
- ssh_auth_key_3 = ssh-rsa AAAAB3NzaC1yc2EAAAADAQ... (public key for instances/vm) -> created local
- ssh_auth_key_4 = ssh-rsa AAAAB3NzaC1yc2EAAAADAQ... (public key for instances/vm) -> created local

# Correct image with shape
If you are struggling with errors like shape is not compatible with image you can reverse engineer it by using this:
```
data "oci_core_images" "ampere-ubuntu-images" {
  compartment_id           = var.compartment_id_var
  operating_system         = "Canonical Ubuntu"
  operating_system_version = "22.04"
  shape                    = "VM.Standard.A1.Flex"
  sort_by                  = "TIMECREATED"
  sort_order               = "DESC"
}
```
This way tf will "grep" them directly from the source, so it can't say it's not compatible

# Budget and alerts
I created a budget and set a alert. This alert will trigger when billing has reached 1 (dependeing on your currency)
- also here in the oci_budget_alert_rule resource you can add email that you want to notify if the budget was reached

# Documentation
- https://docs.oracle.com/en-us/iaas/developer-tutorials/tutorials/tf-provider/01-summary.htm
- https://docs.oracle.com/en-us/iaas/developer-tutorials/tutorials/tf-simple-infrastructure/01-summary.htm
- https://docs.oracle.com/en-us/iaas/developer-tutorials/tutorials/tf-compute/01-summary.htm
- https://registry.terraform.io/providers/oracle/oci/latest/docs