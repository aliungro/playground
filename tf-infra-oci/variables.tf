variable "tenancy_id" {
  type = string
}

variable "user_id" {
  type = string
}

variable "api_fingerprint" {
  type = string
}

variable "region" {
  type = string
}

variable "private_key" {
  type = string
}

variable "compartment_id_var" {
  type = string
}

variable "image_id_oci" {
  type = string
}

variable "subnet_id_oci" {
  type = string
}

variable "ssh_auth_key_1" {
  type = string
}

variable "ssh_auth_key_2" {
  type = string
}

variable "availability_domain_oci_europe_frankfurt_1_ad_2" {
  type = string
}

variable "ssh_auth_key_3" {
  type = string
}

variable "ssh_auth_key_4" {
  type = string
}

variable "source_type_image" {
  type    = string
  default = "image"
}