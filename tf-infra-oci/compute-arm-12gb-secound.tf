resource "oci_core_instance" "ubuntu_instance_4" {
  availability_domain = var.availability_domain_oci_europe_frankfurt_1_ad_2
  compartment_id      = var.compartment_id_var
  shape               = "VM.Standard.A1.Flex"

  shape_config {
    memory_in_gbs = 12
    ocpus         = 2
  }

  source_details {
    source_id   = var.image_id_oci
    source_type = var.source_type_image
  }

  # Optional
  display_name = "ubuntu-arm-12gb-2"

  create_vnic_details {
    assign_public_ip = true
    subnet_id        = var.subnet_id_oci
  }

  metadata = {
    ssh_authorized_keys = var.ssh_auth_key_4
  }

  preserve_boot_volume = false
}