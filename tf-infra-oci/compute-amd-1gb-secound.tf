resource "oci_core_instance" "ubuntu_instance_2" {
  availability_domain = var.availability_domain_oci_europe_frankfurt_1_ad_2
  compartment_id      = var.compartment_id_var
  shape               = "VM.Standard.E2.1.Micro"

  source_details {
    source_id   = var.image_id_oci
    source_type = var.source_type_image
  }

  # Optional
  display_name = "ubuntu-amd-1gb-2"

  create_vnic_details {
    assign_public_ip = true
    subnet_id        = var.subnet_id_oci
  }

  metadata = {
    ssh_authorized_keys = var.ssh_auth_key_2
  }

  preserve_boot_volume = false
}